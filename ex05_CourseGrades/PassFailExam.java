package ex05_CourseGrades;

import ex04_Essay.GradedActivity;

public class PassFailExam extends GradedActivity {
	/*
	 * Fields
	 */
	private boolean passFail;
	private final int NUM_QUESTIONS = 10;
	private int correctTotal;
	
	
	/*
	 * Constructor
	 */
	public PassFailExam(int correct)
	{
		super();
		this.correctTotal = correct;
		setScore(correct);
		setPassFail(this);
	}
	public PassFailExam()
	{
		super();
	}
	
	/*
	 * Mutators
	 */
	public void setPassFail(GradedActivity a)
	{
		if(a.getScore() >= 70)
		{
			this.passFail = true;
		}
		else
			this.passFail = false;
	}
	@Override
	public void setScore(double s)
	{
		this.correctTotal = (int)s;
		double score = 0;
		score = s * 10;
		super.setScore(score);
	}
	
	public void setCorrectTotal(int cor)
	{
		this.correctTotal = cor;
	}
	
	
	/*
	 * Accessors
	 */
	public int getCorrectTotal()
	{
		return this.correctTotal;
	}
	
	public int getNumQuestions()
	{
		return this.NUM_QUESTIONS;
	}
	@Override
	public char getGrade()
	{
		if(this.passFail)
			return 'P';
		else
			return 'F';
	}
	
	/*
	 * toString method
	 */
	@Override
	public String toString()
	{
		String str = "";
		str = String.format("\nPASS FAIL EXAM" +
							"\nPass/Fail: %s" + 
							"\nScore: %.2f",
							this.getGrade(),this.getScore());
		return str;
	}
}
