package ex03;

public class RoomCarpet {
	/*
	 * AGGREGATION
	 */
	private RoomDimension size;
	private double carpetCost;

	// constructors
	public RoomCarpet(RoomDimension dim, double cost) {
		size = dim;
		carpetCost = cost;
	}

	public double calcTotalCost() {
		double total = 0;
		total = size.calcArea() * carpetCost;
		return total;
	}

	public String toString() {
		String str = "";
		str = String.format("\nCost per square foot: $%,.2f" +
							"\nTotal carpet cost: $%,.2f",
							carpetCost, calcTotalCost());
		return str;
	}
}
