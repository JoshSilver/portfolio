package ex08_GradeBook;

public class GradeBook {
	
	private final int ROW = 5;
	private final int COL = 4;
	private String[] students = new String[5];
	private char[] letterGrade = new char[] { 'A', 'B', 'C', 'D', 'F' };
	private double[][] scores = new double[ROW][COL];
	
	
	//Constructor
	public GradeBook(String[] studentNames, double[][] scores)
	{
		students = studentNames;
		this.scores = scores;
	}
	
	//Instance Methods
	public double getAverageTestScore(String name)
	{
		double average = 0;
		int INDEX = 0;
		int sub = 0;
		for(String studentName : students)
		{
			if(studentName.equals(name))
			{
				INDEX = sub;
			}
			sub++;	
		}
		
		double total = 0;
		for(int col = 0; col < scores[INDEX].length; col++ )
		{
			total += scores[INDEX][col];
		}
		average = total / scores[INDEX].length;
		return average;
	}
	
	public char getLetterGrade(String name)
	{
		char letter = 'z';
		double average = getAverageTestScore(name);
		
		if (average >= 90)
			letter = letterGrade[0];
		
		else if(average >= 80)
			letter = letterGrade[1];
		
		else if (average >= 70)
			letter = letterGrade[2];
		
		else if(average >= 60)
			letter = letterGrade[3];
		
		else if (average > 0)
			letter = letterGrade[4];
		
		return letter;
	}
}
