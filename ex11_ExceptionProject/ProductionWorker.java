package ex11_ExceptionProject;

public class ProductionWorker extends Employee {
	/*
	 * fields
	 */
	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	/*
	 * Constructors
	 */
	public ProductionWorker(String n, String num, String date, int shift, double rate) throws InvalidShift, InvalidPayrate, InvalidEmployeeNumber
	{
		super(n, num, date);
		if(shift != 1 || shift !=2)
			throw new InvalidShift();
		this.shift = shift;
		if (rate < 0)
			throw new InvalidPayrate();
		this.payRate = rate;
	}
	public ProductionWorker()
	{
		
	}
	/*
	 * Mutators
	 */
	public void setShift(int s) throws InvalidShift
	{
		if(s != 1 && s !=2)
			throw new InvalidShift();
		this.shift = s;
	}
	public void setPayRate(double p) throws InvalidPayrate
	{
		if(p < 0)
			throw new InvalidPayrate();
		this.payRate = p;
	}
	/*
	 * Accessors
	 */
	public int getShift()
	{
		return this.shift;
	}
	public double getPayRate()
	{
		return this.payRate;
	}
	public String toString()
	{
		String str = "";
		str = String.format(super.toString() +
							"\nShift: %s" + 
							"\nPayrate: $%,.2f", 
							this.getShift(), this.getPayRate());
		return str;
	}
}
