package ex11_ExceptionProject;
/*
 * HANDLING EXCEPTIONS
 */
public class ExceptionProjectDriver {

	public static void main(String[] args) {
		ProductionWorker worker = new ProductionWorker();
		
		worker.setName("Josh Silver");
		try 
		{
			worker.setPayRate(-5);
		} 
		catch (InvalidPayrate e) 
		{
			System.out.println(e.getMessage());
		}
		try 
		{
			worker.setShift(3);
		}
		catch (InvalidShift e) 
		{
			System.out.println(e.getMessage());
		}
		try 
		{
			worker.setEmployeeNumber("4321-z");
		}
		catch (InvalidEmployeeNumber e) 
		{
			System.out.println(e.getMessage());
		}
		worker.setHireDate("12/25/2018");
		
		System.out.println();
		System.out.println(worker.toString());
	}
}
