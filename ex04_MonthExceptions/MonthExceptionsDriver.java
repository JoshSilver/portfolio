package ex04_MonthExceptions;


public class MonthExceptionsDriver {

	public static void main(String[] args) {
		
		Month month = new Month();
		
		//a negative number and a number greater than 12 throws an exception
		try 
		{
			month.setMonthNumber(13);
		} 
		catch (InvalidMonthNumber e) 
		{
			System.out.println(e.getMessage());
		}
		
		//invalid month name throws an exception
		try
		{
			Month month2 = new Month("ja");
		}
		catch(InvalidMonthName e)
		{
			System.out.println(e.getMessage());
		}
	}
}
