package ex04_MonthExceptions;

public class InvalidMonthNumber extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidMonthNumber()
	{
		super("Number for month has to be 1 - 12");
	}

}
