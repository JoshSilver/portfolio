package ex05;

public class MonthDriver {

	public static void main(String[] args) {
		// instantiate first month object
		Month empty = new Month();
		Month month = new Month("February");

		// use getMonthName(), getMonthNumber(), and toString()
		System.out.println(empty.getMonthName());
		System.out.println(empty.getMonthNumber());
		System.out.println(empty.toString());

		// use getMonthName(), getMonthNumber(), and toString()
		System.out.println("\n" + month.getMonthName());
		System.out.println(month.getMonthNumber());
		System.out.println(month.toString());

		System.out.println("");

		// instantiate second month object
		Month month2 = new Month("December");

		// use getMonthName(), getMonthNumber(), and toString()
		System.out.println(month2.getMonthName());
		System.out.println(month2.getMonthNumber());
		System.out.println(month2.toString());

		// use equals(), isGreaterThan(), and isLessThan()
		System.out.println("\nIs month one equal to month two?: " + month.equals(month2));
		System.out.println("Is month one greater than month two?: " + month.isGreaterThan(month2));
		System.out.println("Is month one less than month two?: " + month.isLessThan(month2));
	}

}
