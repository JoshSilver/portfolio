package ex03_ChargeAccountValidation;

public class ValidAccounts {
	private int[] validAccounts = { 5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 
			  						8080152, 4562555, 5552012, 5050552, 7825877, 1250255,
			  						1005231, 6545231, 3852085, 7576651, 7881200, 4581002 };
	
	
	
	
	public boolean isValid(int num)
	{
		selectionSort(validAccounts);
		boolean isValid = false;
		for (int number : validAccounts)
		{
			if (number == num)
			{
				isValid = true;
			}
		}
		return isValid;
	}
	
	public static void selectionSort(int[] array)
	   {
	      int startScan, index, minIndex, minValue;

	      for (startScan = 0; startScan < (array.length-1); startScan++)
	      {
	         minIndex = startScan;
	         minValue = array[startScan];
	         for(index = startScan + 1; index < array.length; index++)
	         {
	            if (array[index] < minValue)
	            {
	               minValue = array[index];
	               minIndex = index;
	            }
	         }
	         array[minIndex] = array[startScan];
	         array[startScan] = minValue;
	      }
	   }
}
