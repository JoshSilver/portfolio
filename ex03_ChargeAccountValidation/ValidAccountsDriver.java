package ex03_ChargeAccountValidation;

import java.util.Scanner;

public class ValidAccountsDriver {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ValidAccounts validAccount = new ValidAccounts();
		boolean isValid = false;
		do
		{
			//ask user for an account # until a valid one is entered
			System.out.println("Enter an account number to see if it's valid: ");
			int accountNum = input.nextInt();
			
			isValid = validAccount.isValid(accountNum);
			
			if(isValid)
			{
				System.out.println("That account is valid");
			}
			else
			{
				System.out.println("That account is NOT valid\n");
			}
		} while(isValid == false);
		input.close();
	}
}
