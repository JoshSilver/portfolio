package ex01;

public class AreaDriver {

	public static void main(String[] args) {
		// area of circle
		System.out.println(Area.calcArea(5));

		// area of rectangle
		System.out.println(Area.calcArea(5.0, 10.0));

		// area of cylinder
		System.out.println(Area.calcArea(5f, 10f));
	}

}
